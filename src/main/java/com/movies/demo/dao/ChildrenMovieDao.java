package com.movies.demo.dao;

import com.movies.demo.model.ChildrenMovie;
import com.movies.demo.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by David on 01 Jun, 2021
 **/
public interface ChildrenMovieDao extends JpaRepository<ChildrenMovie, Long> {
    ChildrenMovie findByMovie(Movie m);
}
