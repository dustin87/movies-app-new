package com.movies.demo.dao;

import com.movies.demo.model.ChildrenMovie;
import com.movies.demo.model.Movie;
import com.movies.demo.model.RegularMovie;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by David on 01 Jun, 2021
 **/
public interface RegularMovieDao extends JpaRepository<RegularMovie, Long> {
    RegularMovie findByMovie(Movie m);
}
