package com.movies.demo.dao;

import com.movies.demo.model.ChildrenMovie;
import com.movies.demo.model.Movie;
import com.movies.demo.model.NewReleaseMovie;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by David on 01 Jun, 2021
 **/
public interface NewReleaseMovieDao extends JpaRepository<NewReleaseMovie, Long> {
    NewReleaseMovie findByMovie(Movie m);
}
