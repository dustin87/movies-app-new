package com.movies.demo.dao;

import com.movies.demo.model.ChildrenMovie;
import com.movies.demo.model.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by David on 01 Jun, 2021
 **/
public interface MovieDao extends JpaRepository<Movie, Long> {
    Boolean existsByTitle(String title);

    Movie findByTitle(String title);

    Page<Movie> findAllByStatusOrderByCreatedAtDesc(Movie.Status status, Pageable pageable);
}
