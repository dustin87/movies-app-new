package com.movies.demo.mngr;

import com.movies.demo.dto.PaginatedListDto;
import com.movies.demo.dto.others.MovieDetailsDto;
import com.movies.demo.dto.others.MovieDto;
import com.movies.demo.dto.request.CalculateMoviePriceRequest;

/**
 * Created by David on 01 Jun, 2021
 **/
public interface MovieMngr {
    PaginatedListDto<MovieDto> getMovies(int page, int limit);

    MovieDetailsDto getMoviePriceDetails(CalculateMoviePriceRequest request);
}
