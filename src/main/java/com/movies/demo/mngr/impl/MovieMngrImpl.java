package com.movies.demo.mngr.impl;

import com.movies.demo.dao.ChildrenMovieDao;
import com.movies.demo.dao.MovieDao;
import com.movies.demo.dao.NewReleaseMovieDao;
import com.movies.demo.dao.RegularMovieDao;
import com.movies.demo.dto.PaginatedListDto;
import com.movies.demo.dto.others.*;
import com.movies.demo.dto.request.CalculateMoviePriceRequest;
import com.movies.demo.exception.ConflictException;
import com.movies.demo.exception.NotFoundException;
import com.movies.demo.mngr.MovieMngr;
import com.movies.demo.model.ChildrenMovie;
import com.movies.demo.model.Movie;
import com.movies.demo.model.NewReleaseMovie;
import com.movies.demo.model.RegularMovie;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by David on 01 Jun, 2021
 **/
@Service
@AllArgsConstructor
@Transactional
public class MovieMngrImpl implements MovieMngr {

    private MovieDao movieDao;
    private RegularMovieDao regularMovieDao;
    private ChildrenMovieDao childrenMovieDao;
    private NewReleaseMovieDao newReleaseMovieDao;
    private ModelMapper modelMapper;

    @Override
    public PaginatedListDto<MovieDto> getMovies(int page, int limit) {

        Page<Movie> pageInfo = movieDao.findAllByStatusOrderByCreatedAtDesc(Movie.Status.ACTIVE,
                PageRequest.of(page, limit));

        List<MovieDto> movies = pageInfo.getContent().stream()
                .map(m -> modelMapper.map(m, MovieDto.class))
                .collect(Collectors.toList());

        return new PaginatedListDto<>(page, limit, pageInfo.getTotalElements(), movies);
    }

    @Override
    public MovieDetailsDto getMoviePriceDetails(CalculateMoviePriceRequest request) {

        // get movie
        Movie  movie = movieDao.findByTitle(request.getMovieTitle());
        if (movie == null) {
            throw new NotFoundException("Movie with this title not found");
        }

        // get more details of the movie
        MovieDetailsDto response = new MovieDetailsDto();
        response.setNameOfUser(request.getUserName());
        response.setMovieInfo(modelMapper.map(movie, MovieDto.class));



        if (movie.getType().equals(Movie.Type.REGULAR)) {
            RegularMovie rm = regularMovieDao.findByMovie(movie);
            if (rm == null)
                throw new NotFoundException("Sorry! Movie details not found");

            response.setRegularMovieDetails(modelMapper.map(rm, RegularMovieDto.class));
            response.setPrice(rm.getPrice() * request.getRentPeriodInDays());
            return response;

        }

        if (movie.getType().equals(Movie.Type.CHILDREN_MOVIE)) {
            ChildrenMovie cm = childrenMovieDao.findByMovie(movie);
            if (cm == null)
                throw new NotFoundException("Sorry! Movie details not found");

            response.setChildrenMovieDetails(modelMapper.map(cm, ChildrenMovieDto.class));
            response.setPrice(
                    (cm.getPrice() * request.getRentPeriodInDays())
                            +  (double)(cm.getMaximumAge()/2));
            return response;
        }

        if (movie.getType().equals(Movie.Type.NEW_RELEASE)) {
            NewReleaseMovie nr = newReleaseMovieDao.findByMovie(movie);
            if (nr == null)
                throw new NotFoundException("Sorry! Movie details not found");

            response.setNewReleaseMovieDetails(modelMapper.map(nr, NewReleaseMovieDto.class));
            response.setPrice(nr.getPrice() * request.getRentPeriodInDays() - nr.getYearReleased());
            return response;
        }

        throw new ConflictException("Movie type error");
    }
}
