package com.movies.demo.dto.others;

import com.movies.demo.model.Movie;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by David on 01 Jun, 2021
 **/
@Data
public class MovieDto {

    private Long id;

    private String title;

    private String type;

    private String genre;

    private String createdAt;
}
