package com.movies.demo.dto.others;

import lombok.Data;

/**
 * Created by David on 01 Jun, 2021
 **/
@Data
public class RegularMovieDto {

    private Long id;

    private Double price;
}
