package com.movies.demo.dto.others;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.movies.demo.model.ChildrenMovie;
import lombok.Data;

/**
 * Created by David on 01 Jun, 2021
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MovieDetailsDto {

    private String nameOfUser;

    private Double price;

    private MovieDto movieInfo;

    private ChildrenMovieDto childrenMovieDetails;

    private RegularMovieDto regularMovieDetails;

    private NewReleaseMovieDto newReleaseMovieDetails;




}
