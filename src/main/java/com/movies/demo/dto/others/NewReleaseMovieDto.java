package com.movies.demo.dto.others;

import com.movies.demo.model.Movie;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Created by David on 01 Jun, 2021
 **/
@Data
public class NewReleaseMovieDto {

    private Long id;

    private Double price;

    private Integer yearReleased;
}
