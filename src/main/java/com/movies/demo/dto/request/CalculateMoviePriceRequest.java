package com.movies.demo.dto.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Created by David on 01 Jun, 2021
 **/
@Data
public class CalculateMoviePriceRequest {

    @NotEmpty(message = "The name of the user is required")
    private String userName;

    @NotEmpty(message = "The name of the movie is required")
    private String movieTitle;

    @NotNull(message = "Rent period is required")
    private Integer rentPeriodInDays;
}
