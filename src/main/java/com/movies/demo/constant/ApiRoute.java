package com.movies.demo.constant;

/**
 * Created by David on 01 Jun, 2021
 **/
public interface ApiRoute {
    String MOVIES = "/movies";

    String PRICING_DETAILS = "/pricing-details";
}
