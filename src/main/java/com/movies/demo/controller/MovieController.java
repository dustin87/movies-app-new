package com.movies.demo.controller;

import com.movies.demo.constant.ApiRoute;
import com.movies.demo.dto.request.CalculateMoviePriceRequest;
import com.movies.demo.mngr.MovieMngr;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by David on 01 Jun, 2021
 **/
@RestController
@AllArgsConstructor
@CrossOrigin
public class MovieController {

    private MovieMngr movieMngr;

    @GetMapping(ApiRoute.MOVIES)
    public ResponseEntity<?> getAllMovies(@RequestParam(defaultValue = "0") int page,
                                          @RequestParam(defaultValue = "10") int limit) {

        return ResponseEntity.ok(movieMngr.getMovies(page, limit));
    }

    @PostMapping(ApiRoute.MOVIES + ApiRoute.PRICING_DETAILS)
    public ResponseEntity<?> getMoviePricingDetails(@RequestBody @Valid CalculateMoviePriceRequest request) {

        return ResponseEntity.ok(movieMngr.getMoviePriceDetails(request));
    }
}
