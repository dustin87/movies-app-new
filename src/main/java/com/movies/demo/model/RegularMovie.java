package com.movies.demo.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by David on 01 Jun, 2021
 **/
@Data
@Entity
public class RegularMovie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double price;

    @OneToOne
    private Movie movie;
}
