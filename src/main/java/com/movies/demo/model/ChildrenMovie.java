package com.movies.demo.model;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by David on 01 Jun, 2021
 **/
@Data
@Entity
public class ChildrenMovie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double price;

    private Integer maximumAge;

    @OneToOne
    private Movie movie;
}
