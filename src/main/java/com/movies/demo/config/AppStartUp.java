package com.movies.demo.config;

import com.movies.demo.dao.ChildrenMovieDao;
import com.movies.demo.dao.MovieDao;
import com.movies.demo.dao.NewReleaseMovieDao;
import com.movies.demo.dao.RegularMovieDao;
import com.movies.demo.model.ChildrenMovie;
import com.movies.demo.model.Movie;
import com.movies.demo.model.NewReleaseMovie;
import com.movies.demo.model.RegularMovie;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

/**
 * Created by David on 01 Jun, 2021
 **/
@Component
@Transactional
@AllArgsConstructor
public class AppStartUp implements ApplicationListener<ApplicationReadyEvent> {

    private MovieDao movieDao;
    private ChildrenMovieDao childrenMovieDao;
    private NewReleaseMovieDao newReleaseMovieDao;
    private RegularMovieDao regularMovieDao;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        createRegularMovies();
        createChildrenMovies();
        createNewReleaseMovies();
    }


    public void createRegularMovies() {
        if (!movieDao.existsByTitle("The Sound of Music")) {
            // create movie 1
            Movie m = new Movie();
            m.setTitle("The Sound of Music");
            m.setGenre(Movie.Genre.DRAMA);
            m.setType(Movie.Type.REGULAR);
            m = movieDao.save(m);

            // create regular-movie 1
            RegularMovie rm = new RegularMovie();
            rm.setMovie(m);
            rm.setPrice(10.0);
            regularMovieDao.save(rm);

            // create movie 2
            m = new Movie();
            m.setTitle("Gone with the Wind");
            m.setGenre(Movie.Genre.ROMANCE);
            m.setType(Movie.Type.REGULAR);
            m = movieDao.save(m);

            // create regular-movie 2
            rm = new RegularMovie();
            rm.setMovie(m);
            rm.setPrice(10.0);
            regularMovieDao.save(rm);
        }
    }

    public void createChildrenMovies() {
        if (!movieDao.existsByTitle("Annie")) {
            // create movie 1
            Movie m = new Movie();
            m.setTitle("Annie");
            m.setGenre(Movie.Genre.COMEDY);
            m.setType(Movie.Type.CHILDREN_MOVIE);
            m = movieDao.save(m);

            // create children-movie 1
            ChildrenMovie cm = new ChildrenMovie();
            cm.setMovie(m);
            cm.setPrice(8.0);
            cm.setMaximumAge(18);
            childrenMovieDao.save(cm);

            // create movie 2
            m = new Movie();
            m.setTitle("Power Rangers");
            m.setGenre(Movie.Genre.COMEDY);
            m.setType(Movie.Type.CHILDREN_MOVIE);
            m = movieDao.save(m);

            // create children-movie 2
            cm = new ChildrenMovie();
            cm.setMovie(m);
            cm.setPrice(8.0);
            cm.setMaximumAge(14);
            childrenMovieDao.save(cm);
        }
    }

    public void createNewReleaseMovies() {
        if (!movieDao.existsByTitle("Avengers: End Game")) {
            // create movie 1
            Movie m = new Movie();
            m.setTitle("Avengers: End Game");
            m.setGenre(Movie.Genre.ACTION);
            m.setType(Movie.Type.NEW_RELEASE);
            m = movieDao.save(m);

            // create new-release-movie 1
            NewReleaseMovie nr = new NewReleaseMovie();
            nr.setMovie(m);
            nr.setPrice(15.0);
            nr.setYearReleased(2019);
            newReleaseMovieDao.save(nr);

            // create movie 2
            m = new Movie();
            m.setTitle("Fast and Furious 9");
            m.setGenre(Movie.Genre.HORROR);
            m.setType(Movie.Type.NEW_RELEASE);
            m = movieDao.save(m);

            // create new-release-movie 2
            nr = new NewReleaseMovie();
            nr.setMovie(m);
            nr.setPrice(15.0);
            nr.setYearReleased(2022);
            newReleaseMovieDao.save(nr);
        }
    }
}
