package com.movies.demo;

import com.movies.demo.dao.ChildrenMovieDao;
import com.movies.demo.dao.MovieDao;
import com.movies.demo.dao.NewReleaseMovieDao;
import com.movies.demo.dao.RegularMovieDao;
import com.movies.demo.dto.others.MovieDetailsDto;
import com.movies.demo.dto.request.CalculateMoviePriceRequest;
import com.movies.demo.exception.NotFoundException;
import com.movies.demo.mngr.impl.MovieMngrImpl;
import com.movies.demo.model.Movie;
import com.movies.demo.model.RegularMovie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

/**
 * Created by David on 05 Oct, 2021
 **/
@RunWith(MockitoJUnitRunner.Silent.class)
public class MovieMngrTest {

    @InjectMocks private MovieMngrImpl movieMngr;
    @Mock private MovieDao movieDao;
    @Mock private RegularMovieDao regularMovieDao;
    @Mock private ChildrenMovieDao childrenMovieDao;
    @Mock private NewReleaseMovieDao newReleaseMovieDao;
    @Mock private ModelMapper modelMapper;

    // test data
    Movie movie = new Movie();
    RegularMovie regularMovie = new RegularMovie();


    @Before
    public void init() {

        movie.setId(2L);
        movie.setType(Movie.Type.REGULAR);
        movie.setGenre(Movie.Genre.DRAMA);
        movie.setTitle("Indiana Jones");
        movie.setStatus(Movie.Status.ACTIVE);


        regularMovie.setId(1L);
        regularMovie.setMovie(movie);
        regularMovie.setPrice(10.0);

    }

    @Test(expected = NotFoundException.class)
    public void getMoviePriceDetailsThrowsException() {
        // expectation
        Mockito.when(movieDao.findByTitle("Indiana Jones 2")).thenReturn(null);

        // test
        CalculateMoviePriceRequest req = new CalculateMoviePriceRequest();
        req.setMovieTitle("Indiana Jones 2");
        req.setRentPeriodInDays(20);
        req.setUserName("Tom Brody");

        movieMngr.getMoviePriceDetails(req);

    }


    @Test
    public void getMoviePriceDetailsSuccessfully() {
        // expectation
        Mockito.when(movieDao.findByTitle("Indiana Jones")).thenReturn(movie);
        Mockito.when(regularMovieDao.findByMovie(movie)).thenReturn(regularMovie);

        // test
        CalculateMoviePriceRequest req = new CalculateMoviePriceRequest();
        req.setMovieTitle("Indiana Jones");
        req.setRentPeriodInDays(20);
        req.setUserName("Tom Brody");

        MovieDetailsDto dto = movieMngr.getMoviePriceDetails(req);
        assert dto.getPrice().equals(200.0);

    }


}

